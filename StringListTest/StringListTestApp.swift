//
//  StringListTestApp.swift
//  StringListTest
//
//  Created by Arseny Gorenkin on 14.11.2021.
//

import SwiftUI

@main
struct StringListTestApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView(viewModel: ViewModel())
        }
    }
}
