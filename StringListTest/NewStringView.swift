import Foundation
import SwiftUI

struct NewStringView: View {

    @Environment(\.presentationMode) var presentationMode
    @State private var text: String = ""

    var receiver: Updatable?

    var body: some View {

        VStack {
            TextField("", text: $text)
                    .textFieldStyle(.roundedBorder)
                    .padding()

            Button(NSLocalizedString("add", comment: ""), action: {
                receiver?.update(text)
                presentationMode.wrappedValue.dismiss()
            }).buttonStyle(.bordered)
        }
    }
}
