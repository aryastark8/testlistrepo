import Foundation

class ViewModel: ObservableObject, Updatable {

    @Published var strings: [String] = []
    private let defKey = "savedData"

    init() {
        strings = UserDefaults.standard.array(forKey: defKey) as? [String] ?? []
    }

    func update(_ value: String) {
        if !value.trim().isEmpty {
            strings.append(value)
            UserDefaults.standard.set(strings, forKey: defKey)
        }
    }
}

protocol Updatable {
    func update(_ value: String)
}