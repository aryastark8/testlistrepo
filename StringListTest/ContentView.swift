//
//  ContentView.swift
//  StringListTest
//
//  Created by Arseny Gorenkin on 14.11.2021.
//

import SwiftUI

struct ContentView: View {

    @ObservedObject var viewModel: ViewModel
    @State var showNewString = false

    var body: some View {
        NavigationView {
            List(viewModel.strings, id: \.self) { string in
                Text(string)

            }.navigationBarItems(trailing:
            Button(action: {
                showNewString = true
            }) {
                NavigationLink(destination: NewStringView(receiver: viewModel)) {
                    Text(NSLocalizedString("add", comment: "add"))
                }
            })
        }
    }
}
