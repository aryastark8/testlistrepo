import Foundation

extension String{

    func trim() -> String{
        trimmingCharacters(in: .whitespacesAndNewlines)
    }
}
